/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

/**
 * Interface defining methods for a FileSystemService.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public interface LocalFileService {

    /**
     * Get a file from the file system. TODO: getFileAsStream
     *
     * @param inputUrl   the path of the file to be retrieved
     * @param outputPath the output path
     * @return an output stream for the file
     * @throws IOException when the file cannot be accessed
     */
    OutputStream getFile(String inputUrl, String outputPath) throws IOException;

    /**
     * Set a file on the filesystem using an input stream.
     *
     * @param inputStream the input stream for the file
     * @param path        the path on the filesystem to store the file
     * @throws IOException when the file cannot be accessed
     */
    void setFile(InputStream inputStream, String path) throws IOException;


    /**
     * Recursively make directories according to a path.
     *
     * @param path string containing the path
     * @throws IOException when the file cannot be accessed
     */
    void mkdir(String path) throws IOException;

    /**
     * Recursively make directories according to a path.
     *
     * @param file the file
     * @throws IOException when the file cannot be accessed
     */
    void mkdir(File file) throws IOException;

    /**
     * Check if the supplied path exists.
     *
     * @param path string containing the path
     * @return true if the path exists
     * @throws IOException when the file cannot be accessed
     */
    boolean hasDir(String path) throws IOException;
}
