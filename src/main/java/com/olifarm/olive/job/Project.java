/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job;

import com.olifarm.olive.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Project.
 */
public class Project {

    private int id;
    private User user;
    private String name;
    private String description;
    private List<JobGroup> jobGroups;

    /**
     * Instantiates a new Project.
     */
    public Project() {
        jobGroups = new ArrayList<>();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public int getUserId() {
        if (user != null) {
            return user.getId();
        }
        return 0;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId) {
        if (user == null) {
            user = new User();
        }
        user.setId(userId);
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets job groups.
     *
     * @return the job groups
     */
    public List<JobGroup> getJobGroups() {
        return jobGroups;
    }

    /**
     * Sets job groups.
     *
     * @param jobGroups the job groups
     */
    public void setJobGroups(List<JobGroup> jobGroups) {
        this.jobGroups = jobGroups;
    }
}
