/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job.cliapplication;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

/**
 * Class for holding parameter information for an application object
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 * @see CliApplication
 */
public class Parameter implements Serializable {

    /**
     * Unique identifier in datasource.
     */
    private int id;

    /**
     * Name of the application.
     * Used to configure the application in the job receiver.
     */
    @NotBlank
    private String name;

    /**
     * Flag.
     */
    private String flag;

    /**
     * A long flag has two dashes (--flag) instead of one (-flag).
     */
    private boolean longFlag;

    /**
     * Type of the parameter.
     * @see ParameterType
     */
    private ParameterType type;

    /**
     * The arguments that come after the flag.
     */
    private List<String> arguments;

    /**
     * Description of the parameter.
     */
    @NotBlank
    private String description;

    /**
     * Number of arguments.
     */
    @Min(0)
    private int argNum;

    /**
     * Argument separator.
     */
    private String argSep;

    /**
     * Level of the parameter.
     *
     * 1. Normal
     * 2. Advanced
     * 3. Expert
     * 0. System (Hidden parameters)
     */
    @Min(0)
    @Max(4)
    private int level;

    private String formGroup;

    /**
     * Extension for file parameters.
     */
    private String extension;

    /**
     * Extra string added between the extension and the filename.
     */
    private String addition;

    /**
     * Filename for file parameters.
     */
    private String filename;

    /**
     * The path of a file on the local filesystem.
     */
    private String localFilePath;

    /**
     * The path of a file on a remote filesystem.
     */
    private String remoteFilePath;

    /**
     * Parameters will be deleted from datasource if true.
     */
    private boolean remove;

    /**
     * Boolean indicating if multiple instances of the same parameter should be allowed.
     */
    private boolean allowMultiple;


    /**
     * Default constructor.
     */
    public Parameter() {

    }

    /**
     * NotBlank
     * Constructor without arguments.
     *
     * @param id          unique id of the parameter
     * @param flag        the flag
     * @param description desctiption
     * @param level       level 1, 2, 3 or 4
     */
    public Parameter(int id, String flag, String description, int level) {
        this.id = id;
        this.flag = flag;
        this.description = description;
        this.level = level;
    }

    /**
     * Constructor with arguments.
     *
     * @param id          unique id of the parameter
     * @param flag        the flag
     * @param arguments   one or more arguments
     * @param description desctiption
     * @param level       level 1, 2, 3 or 4
     */
    public Parameter(int id, String flag, List<String> arguments, String description, int level) {
        this.id = id;
        this.flag = flag;
        this.arguments = arguments;
        this.description = description;
        this.level = level;
    }

    /**
     * Gets form group.
     *
     * @return the form group
     */
    public String getFormGroup() {
        return formGroup;
    }

    /**
     * Sets form group.
     *
     * @param formGroup the form group
     */
    public void setFormGroup(String formGroup) {
        this.formGroup = formGroup;
    }

    /**
     * Gets local file path.
     *
     * @return the local file path
     */
    public String getLocalFilePath() {
        return localFilePath;
    }

    /**
     * Sets local file path.
     *
     * @param localFilePath the local file path
     */
    public void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    /**
     * Gets remote file path.
     *
     * @return the remote file path
     */
    public String getRemoteFilePath() {
        return remoteFilePath;
    }

    /**
     * Sets remote file path.
     *
     * @param remoteFilePath the remote file path
     */
    public void setRemoteFilePath(String remoteFilePath) {
        this.remoteFilePath = remoteFilePath;
    }

    /**
     * Is remove boolean.
     *
     * @return the boolean
     */
    public boolean isRemove() {
        return remove;
    }

    /**
     * Sets remove.
     *
     * @param remove the remove
     */
    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    /**
     * Is long flag boolean.
     *
     * @return the boolean
     */
    public boolean isLongFlag() {
        return longFlag;
    }

    /**
     * Gets long flag.
     *
     * @return the long flag
     */
    public boolean getLongFlag() {
        return longFlag;
    }

    /**
     * Sets long flag.
     *
     * @param longFlag the long flag
     */
    public void setLongFlag(boolean longFlag) {
        this.longFlag = longFlag;
    }

    /**
     * Gets filename.
     *
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Sets filename.
     *
     * @param filename the filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets flag.
     *
     * @return the flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets flag.
     *
     * @param flag the flag
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public ParameterType getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param parameterType the parameter type
     */
    public void setType(ParameterType parameterType) {
        this.type = parameterType;
    }

    /**
     * Gets arguments.
     *
     * @return the arguments
     */
    public List<String> getArguments() {
        return arguments;
    }

    /**
     * Sets arguments.
     *
     * @param arguments the arguments
     */
    public void setArguments(List<String> arguments) {
        this.arguments = arguments;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets level.
     *
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Sets level.
     *
     * @param level the level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets arg num.
     *
     * @return the arg num
     */
    public int getArgNum() {
        return argNum;
    }

    /**
     * Sets arg num.
     *
     * @param argNum the arg num
     */
    public void setArgNum(int argNum) {
        this.argNum = argNum;
    }

    /**
     * Gets extension.
     *
     * @return the extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Sets extension.
     *
     * @param extension the extension
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * Gets addition.
     *
     * @return the addition
     */
    public String getAddition() {
        return addition;
    }

    /**
     * Sets addition.
     *
     * @param addition the addition
     */
    public void setAddition(String addition) {
        this.addition = addition;
    }

    /**
     * Gets arg sep.
     *
     * @return the arg sep
     */
    public String getArgSep() {
        return argSep;
    }

    /**
     * Sets arg sep.
     *
     * @param argSep the arg sep
     */
    public void setArgSep(String argSep) {
        this.argSep = argSep;
    }

    /**
     * Is allow multiple boolean.
     *
     * @return the boolean
     */
    public boolean isAllowMultiple() {
        return allowMultiple;
    }

    /**
     * Sets allow multiple.
     *
     * @param allowMultiple the allow multiple
     */
    public void setAllowMultiple(boolean allowMultiple) {
        this.allowMultiple = allowMultiple;
    }

    /**
     * Get filename. TODO: method is file param
     *
     * @return filename file name from path
     */
    public String getFileNameFromPath() {
        if (    type != ParameterType.OUTPUT_FILE &&
                type != ParameterType.INPUT_FILE  &&
                type != ParameterType.STD_OUT  &&
                type != ParameterType.STD_ERR &&
                type != ParameterType.OUTPUT_ZIP_FILE) {
            return null; // TODO: Illegal state
        }
        if (arguments != null && arguments.size() > 0) {
            String[] args = arguments.get(0).split("/");
            // Get the last argument after splitting on "/"
            return args[args.length-1];
        }
        return null;
    }



    @Override
    public String toString() {
        return "Parameter{" +
                "id=" + id +
                ", flag='" + flag + '\'' +
                ", type=" + type +
                ", arguments=" + arguments +
                ", description='" + description + '\'' +
                ", argNum=" + argNum +
                ", level=" + level +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Parameter)) return false;

        Parameter parameter = (Parameter) o;

        return getId() == parameter.getId();

    }

    @Override
    public int hashCode() {
        return getId();
    }
}
