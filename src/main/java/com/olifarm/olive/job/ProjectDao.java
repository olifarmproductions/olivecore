/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job;

import com.olifarm.olive.user.User;

import java.util.List;

/**
 * Created by obbakker on 21-4-16.
 */
public interface ProjectDao {

    /**
     * Gets projects.
     *
     * @param user the user
     * @return the projects
     */
    List<Project> getProjects(User user);

    /**
     * Gets project.
     *
     * @param user      the user
     * @param projectId the project id
     * @return the project
     */
    Project getProject(User user, int projectId);

    /**
     * Add project int.
     *
     * @param project the project
     * @return the int
     */
    int addProject(Project project);

    /**
     * Update project.
     *
     * @param project the project
     */
    void updateProject(Project project);

    /**
     * Remove project.
     *
     * @param project the project
     */
    void removeProject(Project project);

}
