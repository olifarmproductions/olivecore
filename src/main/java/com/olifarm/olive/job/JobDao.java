/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job;

import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.user.User;

import java.util.List;

/**
 * DAO used for interacting with job related stuff.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public interface JobDao {

    /**
     * Get a list of jobs objects for a user.
     *
     * @param user the user
     * @return list of job objects
     */
    List<Job> getJobs(User user);

    /**
     * Gets jobs by group id.
     *
     * @param user       the user
     * @param jobGroupId the job group id
     * @return the jobs by group id
     */
    List<Job> getJobsByGroupId(User user, int jobGroupId);

    /**
     * Get a list of jobs objects for a user limited to a certain amount.
     *
     * @param user  the user
     * @param limit the limit
     * @return list of job objects
     */
    List<Job> getJobsLimit(User user, int limit);

    /**
     * Gets the maximum user job id for a user.
     *
     * @param user the user
     * @return max user job id
     */
    int getMaxUserJobId(User user);

    /**
     * Get a list of parameter objects for a job.
     *
     * @param job the job
     * @return list of parameter objects
     */
    List<Parameter> getParameters(Job job);

    /**
     * Get a job.
     *
     * @param id   the jobs id
     * @param user the user
     * @return the job
     */
    Job getJob(int id, User user);

    /**
     * Update the status of a job.
     *
     * @param job the job
     * @return boolean indicating success
     */
    boolean updateStatus(Job job);

    /**
     * Update the completion time of a job.
     *
     * @param job the job
     * @return boolean indicating success
     */
    boolean updateStopTime(Job job);

    /**
     * Add a job.
     *
     * @param job the job
     * @return boolean indicating success
     */
    boolean create(Job job);

    /**
     * Add a parameter to a job.
     *
     * @param job       the job
     * @param parameter the parameter
     * @return boolean indicating success
     */
    boolean addParameter(Job job, Parameter parameter);


    /**
     * Sets peregrine job id.
     *
     * @param job the job
     * @return the peregrine job id
     */
    boolean setPeregrineJobId(Job job);

    /**
     * Add multiple parameters to a job.
     *
     * @param job the job
     */
    void setOutputParams(Job job);

    /**
     * Get a parameter object.
     *
     * @param user    the user
     * @param jobId   the jobs id
     * @param paramId the parameters id
     * @return the parameter
     */
    Parameter getParameter(User user, int jobId, int paramId);

    /**
     * Sets job group.
     *
     * @param job the job
     * @return the job group
     */
    boolean setJobGroup(Job job);

    /**
     * Remove job boolean.
     *
     * @param job the job
     * @return the boolean
     */
    boolean removeJob(Job job);



}