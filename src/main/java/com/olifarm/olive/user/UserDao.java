/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.user;

import java.util.List;

/**
 * User data access object interface.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
public interface UserDao {

    /**
     * Gets all the users.
     *
     * @return all the user;
     */
    List<User> getUsers();

    /**
     * Get the user by username.
     *
     * @param username the username
     * @return user object
     */
    User getUser(String username);

    /**
     * Get the user by id.
     *
     * @param id the id
     * @return user object
     */
    User getUserById(int id);

    /**
     * Get the user by email adress.
     *
     * @param email the email adress
     * @return user object
     */
    User getUserByEmail(String email);

    /**
     * Get the id of the user by username.
     *
     * @param username the username
     * @return the id
     */
    int getId(String username);

    /**
     * Checks if username already exists.
     *
     * @param username the username
     * @return true if exists
     */
    boolean exists(String username);

    /**
     * Check if the email already exists.
     *
     * @param email email address
     * @return true if exists
     */
    boolean existsEmail(String email);

    /**
     * Should create the user in the data source.
     *
     * @param user User object
     * @return true if succeeded
     */
    boolean create(User user);

    /**
     * Updates the user credentials.
     *
     * @param user the user object
     * @return true if succeeded
     */
    boolean updateUser(User user);

    /**
     * Changes the password of the user.
     *
     * @param user user object
     * @return true if succeeded
     */
    boolean changePassword(User user);

    /**
     * Changes the peregrine username of the user.
     *
     * @param user user object
     * @return true if succeeded
     */
    boolean changePeregrineUsername(User user);

    /**
     * Sets token.
     *
     * @param user the user
     * @return the token
     */
    boolean setToken(User user);

    /**
     * Sets timestamp.
     *
     * @param user the user
     * @return the timestam
     */
    boolean setTimestamp(User user);
}
