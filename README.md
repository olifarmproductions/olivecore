![](https://bytebucket.org/olifarmproductions/oliveweb/raw/04d8602f0b5d5c578e9b87eccc41f236b3716a07/meta/logo/olive_logo_banner.png?token=bcded4eab03b26e89933abb3ed0bae4504888583)

-------------------



# Olive Core#

This repo contains the code for the OliveCore. OliveCore is the core dependency in the Olive project.
It contains the datamodel that is used for communicating between the node-side application and the
web application and the interfaces for several shared classes.

It does not contain any executable code and is used purely as a library.

# Installation
--------------

## Build from sources

To build the project OliveCore must be added as a jar to the libs folder if it is not yet present. OliveCore can be found [here](https://bitbucket.org/olifarmproductions/olivecore) 

### Intellij

Simply clone the project and import it into Intellij. Then link the gradle root and set it to use the 
'default gradle wrapper'. For Intellij usage see the [manual](https://www.jetbrains.com/idea/help/working-with-gradle-projects.html).
Then open the gradle tab and build the project by pressing the blue refresh button. If the global SDK is not set then
this must be [set](https://www.jetbrains.com/idea/help/configuring-global-project-and-module-sdks.html) to the java 8 JDK.
Run the gradle jar task and the jar will appear in the build/libs folder

### Netbeans

For running the project in Netbeans the Gradle plugin must first be installed. The plugin can be found [here](http://plugins.netbeans.org/plugin/44510/gradle-support).
Once installed go to Team > Git > Clone and clone the repo. For more information see [this](https://netbeans.org/kb/docs/ide/git.html). 
Then let gradle finish building the project. Run the gradle jar task and the jar will appear in the build/libs folder

### Command line

To build the project using the commandline you can use gradle. Spring has an excelent [tutorial](https://spring.io/guides/gs/gradle/) on how to do this so
I wont try to explain it myself. Run the gradle jar task and the jar will appear in the build/libs folder