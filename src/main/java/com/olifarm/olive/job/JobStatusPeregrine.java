/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job;

/**
 * Enum used for setting the status of a job run by peregrine.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public enum JobStatusPeregrine {

    /**
     * Boot fail peregrine status.
     * Job terminated due to launch failure, typically due to a hardware failure (e.g. unable to boot the node
     * or block and the job can not be requeued).
     */
    BOOT_FAIL,

    /**
     * Cancelled peregrine status.
     * Job was explicitly cancelled by the user or system administrator. The job may or may not have been initiated.
     */
    CANCELLED,

    /**
     * Completed peregrine status.
     * Job has terminated all processes on all nodes  with an exit code of zero.
     */
    COMPLETED,

    /**
     * Configuring peregrine status.
     * Job has been allocated resources, but are waiting for them to become ready for use (e.g. booting).
     */
    CONFIGURING,

    /**
     * Completing peregrine status.
     * Job is in the process of completing. Some processes on some nodes may still be active.
     */
    COMPLETING,

    /**
     * Failed peregrine status.
     * Job  terminated  with  non-zero  exit code or other failure condition.
     */
    FAILED,

    /**
     * Node fail peregrine status.
     * Job terminated due to failure of one or more  allocated nodes.
     */
    NODE_FAIL,

    /**
     * Pending peregrine status.
     * Job is awaiting resource allocation.
     */
    PENDING,

    /**
     * Preempted peregrine status.
     * Job terminated due to preemption.
     */
    PREEMPTED,

    /**
     * Running peregrine status.
     * Job currently has an allocation.
     */
    RUNNING,

    /**
     * Special exit peregrine status.
     * The job was requeued in a special state. This state can be set by users, typically in  EpilogSlurmctld,
     * if  the  job  has terminated with a particular exit value.
     */
    SPECIAL_EXIT,

    /**
     * Stopped peregrine status.
     * Job has  an  allocation,  but  execution  has  been stopped   with  SIGSTOP  signal.   CPUS  have  been
     * retained by this job.
     */
    STOPPED,

    /**
     * Suspended peregrine status.
     * Job has an allocation, but execution has been  suspended  and CPUs have been released for other jobs.
     */
    SUSPENDED,

    /**
     * Timeout peregrine status.
     * Job terminated upon reaching its time limit.
     */
    TIMEOUT,

    /**
     * Unknown peregrine status.
     * If the peregrine status cannot be determined.
     */
    UNKNOWN;

}
