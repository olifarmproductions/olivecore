package com.olifarm.olive.job;

import com.olifarm.olive.user.User;

import java.util.List;

/**
 * The interface Job group dao.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public interface JobGroupDao {

    /**
     * Gets job group.
     *
     * @param user the user
     * @param id   the id
     * @return the job group
     */
    JobGroup getJobGroup(User user, int id);

    /**
     * Gets job groups.
     *
     * @param user the user
     * @return the job groups
     */
    List<JobGroup> getJobGroups(User user);

    /**
     * Gets job groups as list.
     *
     * @param user the user
     * @return the job groups as list
     */
    List<JobGroup> getJobGroupsAsList(User user);

    /**
     * Gets job groups.
     *
     * @param project the project
     * @return the job groups
     */
    List<JobGroup> getJobGroups(Project project);

    /**
     * Add job group int.
     *
     * @param jobGroup the job group
     * @return the int
     */
    int addJobGroup(JobGroup jobGroup);

    /**
     * Remove job group.
     *
     * @param jobGroup the job group
     */
    void removeJobGroup(JobGroup jobGroup);

    /**
     * Update job group.
     *
     * @param jobGroup the job group
     */
    void updateJobGroup(JobGroup jobGroup);

}
