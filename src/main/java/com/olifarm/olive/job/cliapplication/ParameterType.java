/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job.cliapplication;

/**
 * The enum Parameter type.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
public enum ParameterType {

    /**
     * Text parameter can be a list.
     */
    TEXT(false),

    /**
     * Input file.
     */
    INPUT_FILE(false),

    /**
     * Output file.
     */
    OUTPUT_FILE(true),

    /**
     * Directory for output files.
     */
    OUTPUT_ZIP_FILE(true),

    /**
     * Directory for output files.
     */
    OUTPUT_DIR(true),

    /**
     * Parameter will hold the standard out of an application.
     */
    STD_OUT(true),

    /**
     * Parameter will hold the standard error of an application.
     */
    STD_ERR(true),

    /**
     * When the parameter is just a flag, without a value.
     */
    BOOLEAN(false);

    private boolean systemParam;

    private ParameterType(boolean systemParam) {
        this.systemParam = systemParam;
    }

    /**
     * Is system param boolean.
     *
     * @return the boolean
     */
    public boolean isSystemParam() {
        return this.systemParam;
    }

}