/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job.cliapplication;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Application class.
 * <p>
 * Used to hold all the application properties including the parameters to execute the application.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
public class CliApplication implements Serializable {

    /**
     * Identifier of the application.
     * Must be unique
     */
    private int id;

    /**
     * Name of the application.
     * May not be blank.
     */
    @NotBlank
    private String name;

    /**
     * List separator for parameters with multiple options.
     * e.g. if the list separator is " " and multiple values are present,
     * the parameters will be formatted as "[Value1] [Value2] etc."
     */
    private String listSeparator;

    /**
     * If a custom view is wanted to display an application at the front-end.
     */
    private boolean customView;

    /**
     * Zip the output directory when a application is done executing.
     */
    private boolean zipOutput;

    /**
     * An application is runnable on Peregrine.
     */
    private boolean peregrineEnabled;


    /**
     * The actual parameters of an application.
     *
     * @see Parameter
     */
    @Valid
    @NotEmpty
    private List<Parameter> parameters;

    /**
     * Modules required for a job to run on Peregrine.
     * String must include version number like expected after the "module load" command
     * in a bash run script (sbatch)
     */
    private List<Module> modules;

    /**
     * Instantiates a new Cli application.
     */
    public CliApplication() {
        modules = new ArrayList<>();
    }

    /**
     * Constructor with application name and parameters.
     *
     * @param name       application name
     * @param parameters parameters
     */
    public CliApplication(String name, List<Parameter> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    /**
     * Converts the parameters that are needed for application execution
     * into a list of flags and arguments, so that ProcessBuilder can use them
     *
     * @return list of parameters and arguments
     */
    public List<String> paramsToClString() {
        int i = 0;
        List<String> paramList = new ArrayList<>();

        for (Parameter param : parameters) {
            if (param.getType() != ParameterType.STD_OUT &&
                    param.getType() != ParameterType.STD_ERR &&
                    param.getType() != ParameterType.OUTPUT_ZIP_FILE) {

                String flag;

                // Add - or -- depending on the flag type
                if (param.getFlag() == null || param.getFlag().equals("")) {
                    paramList.add(this.listToCl(param.getArguments()));
                } else {
                    if (param.isLongFlag()) {
                        flag = "--";
                    } else {
                        flag = "-";
                    }

                    // Add the flag
                    if (param.getArgSep() != null && !param.getArgSep().equals("")) {
                        String flagWithArg = flag + param.getFlag() + param.getArgSep() + this.listToCl(param.getArguments());
                        paramList.add(flagWithArg);
                    } else {
                        paramList.add(flag + param.getFlag());
                        paramList.add(this.listToCl(param.getArguments()));
                    }
                }



                i++;
            }
        }
        return paramList;
    }

    /**
     * Converts a list to a string, or if list size 1 to a string,
     * so it can be used as one argument for process execution.
     *
     * @param list the arguments
     * @return string arguments with separator
     */
    private String listToCl(List<String> list) {
        if (list.size() == 1) {
            return list.get(0);
        } else {
            StringBuilder sb = new StringBuilder();
            int i = 0;

            for (String item : list) {
                if (i != 0) {
                    sb.append(listSeparator);
                }
                sb.append(item);
                i++;
            }
            return sb.toString();
        }
    }

    /**
     * Is custom view boolean.
     *
     * @return the boolean
     */
    public boolean isCustomView() {
        return customView;
    }

    /**
     * Sets custom view.
     *
     * @param customView the custom view
     */
    public void setCustomView(boolean customView) {
        this.customView = customView;
    }

    /**
     * Gets list separator.
     *
     * @return the list separator
     */
    public String getListSeparator() {
        return listSeparator;
    }

    /**
     * Sets list separator.
     *
     * @param listSeparator the list separator
     */
    public void setListSeparator(String listSeparator) {
        this.listSeparator = listSeparator;
    }

    /**
     * Is zip output boolean.
     *
     * @return the boolean
     */
    public boolean isZipOutput() {
        return zipOutput;
    }

    /**
     * Sets zip output.
     *
     * @param zipOutput the zip output
     */
    public void setZipOutput(boolean zipOutput) {
        this.zipOutput = zipOutput;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets parameters.
     *
     * @return the parameters
     */
    public List<Parameter> getParameters() {
        return parameters;
    }

    /**
     * Sets parameters.
     *
     * @param parameters the parameters
     */
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    /**
     * Is peregrine enabled boolean.
     *
     * @return the boolean
     */
    public boolean isPeregrineEnabled() {
        return peregrineEnabled;
    }

    /**
     * Sets peregrine enabled.
     *
     * @param peregrineEnabled the peregrine enabled
     */
    public void setPeregrineEnabled(boolean peregrineEnabled) {
        this.peregrineEnabled = peregrineEnabled;
    }

    /**
     * Gets modules.
     *
     * @return the modules
     */
    public List<Module> getModules() {
        return modules;
    }

    /**
     * Sets modules.
     *
     * @param modules the modules
     */
    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    @Override
    public String toString() {
        return "App{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", listSeparator='" + listSeparator + '\'' +
                ", customView=" + customView +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CliApplication that = (CliApplication) o;

        if (getId() != that.getId()) return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        if (getParameters() != null ? !getParameters().equals(that.getParameters()) : that.getParameters() != null)
            return false;
        return getModules() != null ? getModules().equals(that.getModules()) : that.getModules() == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getParameters() != null ? getParameters().hashCode() : 0);
        result = 31 * result + (getModules() != null ? getModules().hashCode() : 0);
        return result;
    }
}
