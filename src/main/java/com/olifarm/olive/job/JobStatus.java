/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */


package com.olifarm.olive.job;

import java.io.Serializable;

/**
 * Enum used for setting the status of a job. Implements {@link Serializable} so it can be sent over a network.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public enum JobStatus implements Serializable {

    /**
     * Status when a job is being processed.
     */
    PROCESSING,

    /**
     * Status when the job has been processed and system process is running.
     */
    RUNNING,

    /**
     * Status when a job/system process has completed successfully (exit code 0).
     */
    DONE,

    /**
     * Status when a job/system process has completed with errors (exit code greater than 0).
     */
    COMPLETED_WITH_ERROR,

    /**
     * Status when a job/system process has not completed (exit code smaller than 0).
     */
    FATAL_ERROR,

    /**
     * Unknown status.
     * If the status cannot be determined.
     */
    UNKNOWN;


    /**
     * Converts an exit code to a {@link JobStatus} instance.
     *
     * @param exitCode the exit code of a process
     * @return {@link JobStatus} TODO: convertSystemExitCode
     */
    public static JobStatus convertExitCode(int exitCode) {
        if (exitCode < 0) {
            return JobStatus.FATAL_ERROR;
        } else if (exitCode > 0) {
            return JobStatus.COMPLETED_WITH_ERROR;
        } else {
            return JobStatus.DONE;
        }

    }


    /**
     * Convert a peregrine status to a job status.
     *
     * @param status the peregrine status
     * @return the JobStatus
     */
    public static JobStatus convertPeregrineStatus(String status) {

        switch (status) {
            case "BOOT_FAIL":
                // Job terminated due to launch failure, typically due
                // to a hardware failure (e.g. unable to boot the node
                // or block and the job can not be requeued).
                return FATAL_ERROR;

            case "CANCELLED":
                // Job was explicitly cancelled by the user or  system
                // administrator.   The  job  may or may not have been
                // initiated.
                return FATAL_ERROR;

            case "COMPLETED":
                // Job has terminated all processes on all nodes  with
                // an exit code of zero.
                return DONE;

            case "CONFIGURING":
                // Job  has  been allocated resources, but are waiting
                // for them to become ready for use (e.g. booting).
                return PROCESSING;

            case "COMPLETING":
                // Job is in the process of completing. Some processes
                // on some nodes may still be active.
                return RUNNING;

            case "FAILED":
                // Job  terminated  with  non-zero  exit code or other
                // failure condition.
                return COMPLETED_WITH_ERROR;

            case "NODE_FAIL":
                // Job terminated due to failure of one or more  allo-
                // cated nodes.
                return FATAL_ERROR;

            case "PENDING":
                // Job is awaiting resource allocation.
                return PROCESSING;

            case "PREEMPTED":
                // Job terminated due to preemption.
                return FATAL_ERROR;

            case "RUNNING":
                // Job currently has an allocation.
                return RUNNING;

            case "SPECIAL_EXIT":
                // The job was requeued in a special state. This state
                // can be set by users, typically in  EpilogSlurmctld,
                // if  the  job  has terminated with a particular exit
                // value.
                return RUNNING;

            case "STOPPED":
                // Job has  an  allocation,  but  execution  has  been
                // stopped   with  SIGSTOP  signal.   CPUS  have  been
                // retained by this job.
                return FATAL_ERROR;

            case "SUSPENDED":
                // Job has an allocation, but execution has been  sus-
                // pended  and CPUs have been released for other jobs.
                return RUNNING;

            case "TIMEOUT":
                // Job terminated upon reaching its time limit.
                return FATAL_ERROR;

            default:
                return UNKNOWN;
        }
    }
}
