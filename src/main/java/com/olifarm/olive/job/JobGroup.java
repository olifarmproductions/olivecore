/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */
package com.olifarm.olive.job;

import com.olifarm.olive.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Job group.
 */
public class JobGroup {

    private int id;
    private int projectId;
    private int parentId;
    private User user;
    private String name;
    private String description;
    private List<Job> jobs;
    private List<JobGroup> children;

    /**
     * Instantiates a new Job group.
     */
    public JobGroup() {
        jobs = new ArrayList<>();
        children = new ArrayList<>();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets project id.
     *
     * @return the project id
     */
    public int getProjectId() {
        return projectId;
    }

    /**
     * Sets project id.
     *
     * @param projectId the project id
     */
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    /**
     * Gets parent id.
     *
     * @return the parent id
     */
    public int getParentId() {
        return parentId;
    }

    /**
     * Sets parent id.
     *
     * @param parentId the parent id
     */
    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public int getUserId() {
        if (user != null) {
            return user.getId();
        }
        return 0;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId) {
        if (user == null) {
            user = new User();
        }
        user.setId(userId);
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets jobs.
     *
     * @return the jobs
     */
    public List<Job> getJobs() {
        return jobs;
    }

    /**
     * Sets jobs.
     *
     * @param jobs the jobs
     */
    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    /**
     * Add job.
     *
     * @param job the job
     */
    public void addJob(Job job) {
        jobs.add(job);
    }

    /**
     * Gets children.
     *
     * @return the children
     */
    public List<JobGroup> getChildren() {
        return children;
    }

    /**
     * Sets children.
     *
     * @param children the children
     */
    public void setChildren(List<JobGroup> children) {
        this.children = children;
    }

    /**
     * Add child.
     *
     * @param jobGroup the job group
     */
    public void addChild(JobGroup jobGroup) {
        children.add(jobGroup);
    }

    /**
     * Remove child.
     *
     * @param jobGroup the job group
     */
    public void removeChild(JobGroup jobGroup) {
        //TODO: test this
        children.remove(jobGroup);
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Has children boolean.
     *
     * @return the boolean
     */
    public boolean hasChildren() {
        return children.size() > 0;
    }
}
