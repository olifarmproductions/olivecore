/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.io;

import com.olifarm.olive.job.Job;

/**
 * Created by hbrugge on 18-2-16.
 */
public interface FilenameService {

    /**
     * Make local input dir string.
     *
     * @param job the job
     * @return the string
     */
    String makeLocalInputDir(Job job);

    /**
     * Make local output dir string.
     *
     * @param job the job
     * @return the string
     */
    String makeLocalOutputDir(Job job);

    /**
     * Make remote input dir string.
     *
     * @param job the job
     * @return the string
     */
    String makeRemoteInputDir(Job job);

    /**
     * Make remote output dir string.
     *
     * @param job the job
     * @return the string
     */
    String makeRemoteOutputDir(Job job);

    /**
     * Create filename string.
     *
     * @param job       the job
     * @param extension the extension
     * @param file_nr   the file nr
     * @return the string
     */
    String createFilename(Job job, String extension, int file_nr);

}


