/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job;

import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.user.User;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;

/**
 * Class containing all information needed to run and complete a job.
 * Implements {@link Serializable} so it can be sent over a network.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public class Job implements Serializable {

    /**
     * Id of the job.
     * Must be unique
     */
    private int id;

    /**
     * Name of the job.
     * Must be unique
     */
    @NotBlank
    private String name;

    /**
     * The id of the group the job belongs to.
     */
    private int jobGroupId;

    /**
     * {@link JobStatus} object containing the current status of the job.
     */
    private JobStatus status;

    /**
     * {@link com.olifarm.olive.job.cliapplication.CliApplication} object containing which app to execute.
     */
    private CliApplication app;

    /**
     * Boolean indicating if the job was run using peregrine.
     */
    private boolean peregrineEnabled;

    /**
     * The id of the job in peregrine.
     */
    private int peregrineJobId;

    /**
     * The status of the job on peregrine.
     */
    private JobStatusPeregrine peregrineJobStatus;

    /**
     * The owner of the job.
     */
    private User user;

    /**
     * The id of the job for the user who made it.
     */
    private int usersJobId;

    /**
     * Id of the user who created the job.
     */
    private int userId;

    /**
     * Date the job was started.
     */
    private Date startTime;

    /**
     * Date the job was completed.
     */
    private Date stopTime;

    /**
     * Boolean indicating if the user should be notified by email when the job has been completed.
     */
    private boolean mailNotification;

    /**
     * Default constructor.
     */
    public Job() {
        this.user = new User();
    }

    /**
     * Constructor.
     *
     * @param app  {@link CliApplication} object containing the app to run the job with
     * @param name the name of the job
     * @param id   the id of the job
     */
    public Job(CliApplication app, String name, int id) {
        this.user = new User();

        this.app = app;
        this.name = name;
        this.id = id;
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets job group id.
     *
     * @return the job group id
     */
    public int getJobGroupId() {
        return jobGroupId;
    }

    /**
     * Sets job group id.
     *
     * @param jobGroupId the job group id
     */
    public void setJobGroupId(int jobGroupId) {
        this.jobGroupId = jobGroupId;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public JobStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(JobStatus status) {
        this.status = status;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets users job id.
     *
     * @return the users job id
     */
    public int getUsersJobId() {
        return usersJobId;
    }

    /**
     * Sets users job id.
     *
     * @param usersJobId the users job id
     */
    public void setUsersJobId(int usersJobId) {
        this.usersJobId = usersJobId;
    }

    /**
     * Gets start time.
     *
     * @return the start time
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Sets start time.
     *
     * @param startTime the start time
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets stop time.
     *
     * @return the stop time
     */
    public Date getStopTime() {
        return stopTime;
    }

    /**
     * Sets stop time.
     *
     * @param stopTime the stop time
     */
    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    /**
     * Is mail notification boolean.
     *
     * @return the boolean
     */
    public boolean isMailNotification() {
        return mailNotification;
    }

    /**
     * Sets mail notification.
     *
     * @param mailNotification the mail notification
     */
    public void setMailNotification(boolean mailNotification) {
        this.mailNotification = mailNotification;
    }

    /**
     * Gets app.
     *
     * @return the app
     */
    public CliApplication getApp() {
        return app;
    }

    /**
     * Sets app.
     *
     * @param app the app
     */
    public void setApp(CliApplication app) {
        this.app = app;
    }

    /**
     * Is peregrine enabled boolean.
     *
     * @return the boolean
     */
    public boolean isPeregrineEnabled() {
        return peregrineEnabled;
    }

    /**
     * Sets peregrine enabled.
     *
     * @param peregrineEnabled the peregrine enabled
     */
    public void setPeregrineEnabled(boolean peregrineEnabled) {
        this.peregrineEnabled = peregrineEnabled;
    }

    /**
     * Gets peregrine job id.
     *
     * @return the peregrine job id
     */
    public int getPeregrineJobId() {
        return peregrineJobId;
    }

    /**
     * Sets peregrine job id.
     *
     * @param peregrineJobId the peregrine job id
     */
    public void setPeregrineJobId(int peregrineJobId) {
        this.peregrineJobId = peregrineJobId;
    }

    /**
     * Gets peregrine job status.
     *
     * @return the peregrine job status
     */
    public JobStatusPeregrine getPeregrineJobStatus() {
        return peregrineJobStatus;
    }

    /**
     * Sets peregrine job status.
     *
     * @param peregrineJobStatus the peregrine job status
     */
    public void setPeregrineJobStatus(JobStatusPeregrine peregrineJobStatus) {
        this.peregrineJobStatus = peregrineJobStatus;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return user.getUsername();
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.user.setUsername(username);
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public int getUserId() {
        return user.getId();
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId) {
        this.user.setId(userId);
    }

    /**
     * Sets application id.
     *
     * @param id the id
     */
    public void setApplicationId(int id) {
        if (app == null) {
            app = new CliApplication();
        }
        this.app.setId(id);
    }

    /**
     * Gets application id.
     *
     * @return the application id
     */
    public int getApplicationId() {
        return app.getId();
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }

    @Override
    public boolean equals(Object o) {
        Job job = (Job) o;
        return this.id == job.getId();

    }

    @Override
    public int hashCode() {
        int result = 31 * id;
        return result;
    }


}