/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.job.cliapplication;

import java.util.List;

/**
 * Application data access object
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
public interface CliApplicationDao {

    /**
     * Gets an application with the corresponding application id.
     *
     * @param id the application id
     * @return the application object
     */
    CliApplication getApplication(int id);

    /**
     * Get the input parameters of an application.
     *
     * @param app the application object
     * @return the input parameters
     */
    List<Parameter> getInputParameters(CliApplication app);


    /**
     * Gets input file parameters.
     *
     * @param app the app
     * @return the input file parameters
     */
    List<Parameter> getInputFileParameters(CliApplication app);

    /**
     * Gets all the applications in the datasource.
     *
     * @return application objects
     */
    List<CliApplication> getApplications();

    /**
     * Gets the output parameters (typically file parameters).
     *
     * @param app the application
     * @return the output parameters
     */
    List<Parameter> getOutputParams(CliApplication app);

    /**
     * Retrieves all of the peregrine modules needed to run an application.
     *
     * @param applicationId the id of the application
     * @return A list of module names
     */
    List<Module> getPeregrineModulesByAppId(int applicationId);

    /**
     * Retrieves all of the available peregrine modules.
     *
     * @return A list of module names
     */
    List<Module> getPeregrineModules();

    /**
     * Set the app in the datasource.
     *
     * @param app app to be added
     * @return identifier in the datasource
     */
    int setApp(CliApplication app);

    /**
     * Set the parameters of the application in the datasource.
     * Needed when the application is set to datasource or parameters are added.
     *
     * @param app the app
     */
    void setAppParams(CliApplication app);

    /**
     * Add the standard out to de datasource.
     *
     * @param app the app
     */
    void setStdOut(CliApplication app);

    /**
     * Add the standard error to de datasource.
     *
     * @param app the app
     */
    void setStdErr(CliApplication app);

    /**
     * Update app information.
     *
     * @param app the app
     */
    void updateApp(CliApplication app);

    /**
     * Check if the parameter already exists in the datasource.
     *
     * @param parameter the parameter
     * @return true if exists
     */
    boolean existsParameter(Parameter parameter);

    /**
     * Update parameter information;
     *
     * @param parameter the parameter
     */
    void updateParameter(Parameter parameter);

    /**
     * Add new parameter.
     *
     * @param app       the app
     * @param parameter the parameter
     */
    void addParameter(CliApplication app, Parameter parameter);

    /**
     * Remove existing parameter.
     *
     * @param parameter the parameter
     */
    void removeParameter(Parameter parameter);


    /**
     * Add peregrine modules to the database for the specified application.
     *
     * @param application The application to add the modules to
     * @param modules     the modules
     */
    void addModules(CliApplication application, List<Module> modules);

    /**
     * Remove peregrine modules to the database for the specified application.
     *
     * @param application The application to remove the modules from
     * @param modules     the modules
     */
    void removeModules(CliApplication application, List<Module> modules);

    /**
     * Add peregrine module to the database.
     *
     * @param module Module object
     */
    void setPeregrineModule(Module module);

    /**
     * Remove peregrine module from the database.
     *
     * @param module Module object
     */
    void removePeregrineModule(Module module);

}
