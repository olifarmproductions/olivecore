/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface defining methods for a FtpService. TODO: should maybe extend file service
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public interface RemoteFileService {

    /**
     * Get a file from the ftp server and set it on the local filesystem.
     *
     * @param inputUrl   path of the file on the ftp server
     * @param outputPath path of the file on the local filesystem
     * @return the file
     * @throws IOException when the file cannot be accessed
     */
    OutputStream getFile(String inputUrl, String outputPath) throws IOException;

    /**
     * Reads a file on the given output stream, directly from the FTP server.
     *
     * @param inputUrl     the url the the file
     * @param outputStream the output stream to be used
     * @throws IOException the io exception
     */
    void read(String inputUrl, OutputStream outputStream) throws IOException;

    /**
     * Set a file from the local filesystem on the ftp server.
     *
     * @param inputStream input stream of the local file
     * @param path        the path on the ftp server
     * @throws IOException when the file cannot be accessed
     */
    void setFile(InputStream inputStream, String path) throws IOException;

    /**
     * Recursively make directories on the ftp server.
     *
     * @param path the directory tree to create.
     * @throws IOException when the path cannot be created
     */
    void mkdir(String path) throws IOException;

    /**
     * Check if a directory exists on the ftp server
     *
     * @param path the directory
     * @return true if the directory exists
     * @throws IOException when the file cannot be accessed
     */
    boolean hasDir(String path) throws IOException;


    /**
     * Copy.
     *
     * @param source the source
     * @param dest   the dest
     * @throws IOException the io exception
     */
    void copy(String source, String dest) throws IOException;

    /**
     * Open a ftp connection.
     */
    void open();

    /**
     * Close a ftp connection.
     */
    void close();
}
