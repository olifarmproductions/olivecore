/*
 *     OliveCore is the core library in the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.user;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * User class.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
public class User implements Serializable {

    /**
     * Contain validation params.
     * Needed for validation groups (annotation)
     */
    public interface ValidationBasic {
    }

    /**
     * Contain validation params.
     * Needed for validation groups (annotation)
     */
    public interface ValidationPassword {
    }

    /**
     * Contain validation params.
     * Needed for validation groups (annotation)
     */
    public interface ValidationEmail {
    }

    /**
     * The interface Validation recover.
     */
    public interface ValidationRecover {

    }

    /**
     * Unique database identifier.
     */
    @Min(value = 0, groups={ValidationRecover.class})
    private int id;

    /**
     * Username.
     */
    @NotBlank(groups = {ValidationBasic.class})
    @Size(min = 3, max = 15, groups = {ValidationBasic.class})
    @Pattern(regexp = "^\\w{3,}$", groups = {ValidationBasic.class})
    private String username;

    /**
     * The authority of the user.
     */
    private String authority;

    /**
     * The first name of the user.
     */
    @NotBlank(groups = {ValidationBasic.class})
    private String name;

    /**
     * The last name of the user.
     */
    @NotBlank(groups = {ValidationBasic.class})
    private String surname;

    /**
     * The email address of the user.
     */
    @NotBlank(groups = {ValidationBasic.class, ValidationEmail.class})
    @Email(groups = {ValidationBasic.class, ValidationEmail.class})
    private String email;

    /**
     * Password of the user. will be encoded before storing in data source.
     */
    @NotBlank(groups = {ValidationPassword.class})
    @Pattern(regexp = "^\\S+$", groups = {ValidationPassword.class})
    @Size(min = 8, max = 15, groups = {ValidationPassword.class})
    private String password;

    /**
     * Is the user enables.
     */
    private boolean enabled;

    /**
     * Password token for password reset.
     * Not yet implemented
     */
    @NotBlank(groups={ValidationRecover.class})
    private String passwordToken;

    /**
     * Time stamp when user is created.
     * Not yet implemented
     */
    private Date timestamp;

    /**
     * The users perigrine username.
     */
    private String peregrineUsername;

    /**
     * The users perigrine password.
     */
    private String peregrinePassword;

    /**
     * Instantiates a new User.
     */
    public User() {

    }

    /**
     * Constructor with basic credentials.
     *
     * @param enabled   true if enabled
     * @param username  the username
     * @param authority the authority
     * @param name      the nameperegrineUsername
     * @param surname   the surname
     * @param email     the email address
     * @param password  the password
     */
    public User(boolean enabled, String username, String authority, String name,
                String surname, String email, String password) {
        this.enabled = enabled;
        this.username = username;
        this.authority = authority;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets authority.
     *
     * @return the authority
     */
    public String getAuthority() {
        return authority;
    }

    /**
     * Sets authority.
     *
     * @param authority the authority
     */
    public void setAuthority(String authority) {
        this.authority = authority;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Is enabled boolean.
     *
     * @return the boolean
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets enabled.
     *
     * @param enabled the enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets password token.
     *
     * @return the password token
     */
    public String getPasswordToken() {
        return passwordToken;
    }

    /**
     * Sets password token.
     *
     * @param passwordToken the password token
     */
    public void setPasswordToken(String passwordToken) {
        this.passwordToken = passwordToken;
    }

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets peregrine username.
     *
     * @return the peregrine username
     */
    public String getPeregrineUsername() {
        return peregrineUsername;
    }

    /**
     * Sets peregrine username.
     *
     * @param peregrineUsername the peregrine username
     */
    public void setPeregrineUsername(String peregrineUsername) {
        this.peregrineUsername = peregrineUsername;
    }

    /**
     * Gets peregrine password.
     *
     * @return the peregrine password
     */
    public String getPeregrinePassword() {
        return peregrinePassword;
    }

    /**
     * Sets peregrine password.
     *
     * @param peregrinePassword the peregrine password
     */
    public void setPeregrinePassword(String peregrinePassword) {
        this.peregrinePassword = peregrinePassword;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", authority='" + authority + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", passwordToken='" + passwordToken + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
